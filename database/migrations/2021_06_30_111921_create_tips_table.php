<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tips', function (Blueprint $table) {
            $table->id();
            $table->string('ticker');
            $table->string('type');
            $table->date('expiration_date');
            $table->integer('strike');
            $table->integer('last_option_price');
            $table->integer('underlying_price');
            $table->bigInteger('volume');
            $table->bigInteger('open_interest');
            $table->integer('volume_open_interest');
            $table->bigInteger('volume_usd');
            $table->bigInteger('volume_on_ask');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tips');
    }
}
