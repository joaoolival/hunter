<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class PlanYearlySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('plans')->insert(
            [
                [
                    'name' => 'Bronze Plan',
                    'price' => '9999',
                    'stripe_plan_id' => 'price_1J4on7G50fch1DJHoOG1VFAw',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'billing_period' => 'year',
                ],

                [
                    'name' => 'Silver Plan',
                    'price' => '19999',
                    'stripe_plan_id' => 'price_1J4onqG50fch1DJH6YBufoGg',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'billing_period' => 'year',
                ],

                [
                    'name' => 'Gold Plan',
                    'price' => '29999',
                    'stripe_plan_id' => 'price_1J4ooIG50fch1DJHAPjme3Ug',
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'billing_period' => 'year',
                ]
            ]
        );
    }
}
