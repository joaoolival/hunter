<?php

return [
    /*
     * Stripe will sign each webhook using a secret. You can find the used secret at the
     * webhook configuration settings: https://dashboard.stripe.com/account/webhooks.
     */
    'signing_secret' => env('STRIPE_WEBHOOK_SECRET'),

    /*
     * You can define the job that should be run when a certain webhook hits your application
     * here. The key is the name of the Stripe event type with the `.` replaced by a `_`.
     *
     * You can find a list of Stripe webhook types here:
     * https://stripe.com/docs/api#event_types.
     */
    'jobs' => [
        'product_created' => \App\Jobs\StripeWebhooks\Products\ProductCreated::class,
        'product_updated' => \App\Jobs\StripeWebhooks\Products\ProductUpdated::class,
        'product_deleted' => \App\Jobs\StripeWebhooks\Products\ProductDeleted::class,

        'price_created' => \App\Jobs\StripeWebhooks\Prices\PriceCreated::class,
        'price_updated' => \App\Jobs\StripeWebhooks\Prices\PriceUpdated::class,
        'price_deleted' => \App\Jobs\StripeWebhooks\Prices\PriceDeleted::class,

        'charge_succeeded' => \App\Jobs\StripeWebhooks\Charges\ChargeSucceededJob::class,

        'customer_updated' => \App\Jobs\StripeWebhooks\Customer\CustomerUpdated::class,
        'customer_deleted' => \App\Jobs\StripeWebhooks\Customer\CustomerDeleted::class,

        'customer_subscription_deleted' => \App\Jobs\StripeWebhooks\Customer\Subscription\CustomerSubscriptionDeleted::class,
        'customer_subscription_updated' => \App\Jobs\StripeWebhooks\Customer\Subscription\CustomerSubscriptionUpdated::class,
        'customer_subscription_created' => \App\Jobs\StripeWebhooks\Customer\Subscription\CustomerSubscriptionCreated::class,

        'invoice_payment_action_required' => \App\Jobs\StripeWebhooks\Invoice\PaymentActionRequired::class,
        'invoice.payment_failed' => \App\Jobs\StripeWebhooks\Invoice\PaymentFailed::class,
    ],

    /*
     * The classname of the model to be used. The class should equal or extend
     * Spatie\StripeWebhooks\ProcessStripeWebhookJob.
     */
    'model' => \Spatie\StripeWebhooks\ProcessStripeWebhookJob::class,

    /*
     * When disabled, the package will not verify if the signature is valid.
     * This can be handy in local environments.
     */
    'verify_signature' => env('STRIPE_SIGNATURE_VERIFY', true),
];
