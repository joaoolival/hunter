<?php

namespace App\Jobs\StripeWebhooks\Invoice;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\WebhookClient\Models\WebhookCall;
use Illuminate\Support\Facades\Log;
use Laravel\Cashier\Payment;
use Stripe\PaymentIntent as StripePaymentIntent;
use Illuminate\Notifications\Notifiable;




class PaymentFailed implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        Log::Alert("payment failed");

        /*if (is_null($notification = config('cashier.payment_notification'))) {
            Log::Alert("payment failed success");
            return null;
        }

        $payload = $this->webhookCall->payload['data']['object'];

        if ($user = User::where('stripe_id', $payload['customer'])->first()) {
            if (in_array(Notifiable::class, class_uses_recursive($user))) {
                $payment = new Payment(StripePaymentIntent::retrieve(
                    $payload['data']['object']['payment_intent'],
                    $user->stripeOptions()
                ));

                $user->notify(new $notification($payment));
                Log::Alert("payment failed alerta");
            }
        }*/
    }
}
