<?php

namespace App\Jobs\StripeWebhooks\Products;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;


class ProductCreated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $product = $this->webhookCall->payload['data']['object'];
        if ($product) {

            $toInclude = ($product['metadata']['include'] ?? NULL);
            $toNotInclude = ($product['metadata']['not_include'] ?? NULL);

            $newProduct = Product::create([
                'id_stripe_product' => $product['id'],
                'name' => $product['name'],
                'active' => $product['active'],
                'include' => ($toInclude != NULL) ? explode(',', $toInclude) : NULL,
                'not_include' => ($toNotInclude != NULL) ? explode(',', $toNotInclude) : NULL,
            ]);
        }
    }
}
