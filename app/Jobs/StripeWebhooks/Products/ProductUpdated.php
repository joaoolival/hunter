<?php

namespace App\Jobs\StripeWebhooks\Products;

use App\Models\Product;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;

class ProductUpdated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $product = $this->webhookCall->payload['data']['object'];
        $productToUpdate = Product::where('id_stripe_product', $product['id'])->first();

        if ($productToUpdate && $product) {
            $productToUpdate->name = $product['name'];
            $productToUpdate->active = $product['active'];

            $toInclude = ($product['metadata']['include'] ?? NULL);
            $productToUpdate->include = ($toInclude != NULL) ? explode(',', $toInclude) : NULL;

            $toNotInclude = ($product['metadata']['not_include'] ?? NULL);
            $productToUpdate->not_include = ($toNotInclude != NULL) ? explode(',', $toNotInclude) : NULL;
            $productToUpdate->save();
        }
    }
}
