<?php

namespace App\Jobs\StripeWebhooks\Customer\Subscription;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\WebhookClient\Models\WebhookCall;
use Laravel\Cashier\Subscription;
use Illuminate\Support\Carbon;
use Stripe\Subscription as StripeSubscription;
use Illuminate\Support\Facades\Log;


class CustomerSubscriptionUpdated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $payload = $this->webhookCall->payload['data']['object'];

        if ($user = User::where('stripe_id', $payload['customer'])->first()) {

            $user->subscriptions->filter(function (Subscription $subscription) use ($payload) {
                return $subscription->stripe_id === $payload['id'];
            })->each(function (Subscription $subscription) use ($payload) {
                if (
                    isset($payload['status']) &&
                    $payload['status'] === StripeSubscription::STATUS_INCOMPLETE_EXPIRED
                ) {
                    $subscription->items()->delete();
                    $subscription->delete();

                    return;
                }

                $firstItem = $payload['items']['data'][0];
                $isSinglePlan = count($payload['items']['data']) === 1;

                // Plan...
                $subscription->stripe_plan = $isSinglePlan ? $firstItem['plan']['id'] : null;

                // Quantity...
                $subscription->quantity = $isSinglePlan && isset($firstItem['quantity']) ? $firstItem['quantity'] : null;

                // Trial ending date...
                if (isset($payload['trial_end'])) {
                    $trialEnd = Carbon::createFromTimestamp($payload['trial_end']);

                    if (!$subscription->trial_ends_at || $subscription->trial_ends_at->ne($trialEnd)) {
                        $subscription->trial_ends_at = $trialEnd;
                    }
                }

                // Cancellation date...
                if (isset($payload['cancel_at_period_end'])) {
                    if ($payload['cancel_at_period_end']) {
                        $subscription->ends_at = $subscription->onTrial()
                            ? $subscription->trial_ends_at
                            : Carbon::createFromTimestamp($payload['current_period_end']);
                    } elseif (isset($payload['cancel_at'])) {
                        $subscription->ends_at = Carbon::createFromTimestamp($payload['cancel_at']);
                    } else {
                        $subscription->ends_at = null;
                    }
                }

                // Status...
                if (isset($payload['status'])) {
                    $subscription->stripe_status = $payload['status'];
                }

                $subscription->save();

                // Update subscription items...
                if (isset($payload['items'])) {
                    $plans = [];

                    foreach ($payload['items']['data'] as $item) {
                        $plans[] = $item['plan']['id'];

                        $subscription->items()->updateOrCreate([
                            'stripe_id' => $item['id'],
                        ], [
                            'stripe_plan' => $item['plan']['id'],
                            'quantity' => $item['quantity'] ?? null,
                        ]);
                    }

                    // Delete items that aren't attached to the subscription anymore...
                    $subscription->items()->whereNotIn('stripe_plan', $plans)->delete();
                }
            });
        }
    }
}
