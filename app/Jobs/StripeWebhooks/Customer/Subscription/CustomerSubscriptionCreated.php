<?php

namespace App\Jobs\StripeWebhooks\Customer\Subscription;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Spatie\WebhookClient\Models\WebhookCall;
use Illuminate\Support\Facades\Log;
use Carbon\Carbon;
use Laravel\Cashier\Cashier;


class CustomerSubscriptionCreated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $payload = $this->webhookCall->payload['data']['object'];

        $user = Cashier::findBillable($payload['customer']);

        if ($user) {

            if (!$user->subscriptions->contains('stripe_id', $payload['id'])) {
                if (isset($data['trial_end'])) {
                    $trialEndsAt = Carbon::createFromTimestamp($data['trial_end']);
                } else {
                    $trialEndsAt = null;
                }

                $firstItem = $payload['items']['data'][0];
                $isSinglePlan = count($payload['items']['data']) === 1;

                $subscription = $user->subscriptions()->create([
                    'name' => $payload['metadata']['name'] ?? 'default',
                    'stripe_id' => $payload['id'],
                    'stripe_status' => $payload['status'],
                    'stripe_plan' => $isSinglePlan ? $firstItem['plan']['id'] : null,
                    'quantity' => $isSinglePlan && isset($firstItem['quantity']) ? $firstItem['quantity'] : null,
                    'trial_ends_at' => $trialEndsAt,
                    'ends_at' => null,
                ]);

                foreach ($payload['items']['data'] as $item) {
                    $subscription->items()->create([
                        'stripe_id' => $item['id'],
                        'stripe_plan' => $item['plan']['id'],
                        'quantity' => $item['quantity'] ?? null,
                    ]);
                }
            }
        }
    }
}
