<?php

namespace App\Jobs\StripeWebhooks\Prices;

use App\Models\Product;
use App\Models\Price;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;


class PriceCreated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $price = $this->webhookCall->payload['data']['object'];
        $product =  Product::where('id_stripe_product', $price['product'])->first();

        if ($product) {
            $newPrice = Price::create([
                'stripe_price_id' => $price['id'],
                'product_id' => $product->id,
                'active' => $price['active'],
                'currency' => $price['currency'],
                'nickname' => $price['nickname'],
                'price' => $price['unit_amount'],
                'billing_period' => $price['recurring']['interval'],
            ]);
        }
    }
}
