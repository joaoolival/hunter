<?php

namespace App\Jobs\StripeWebhooks\Prices;

use App\Models\Price;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Spatie\WebhookClient\Models\WebhookCall;

class PriceUpdated implements ShouldQueue
{
    use InteractsWithQueue, Queueable, SerializesModels;
    /** @var \Spatie\WebhookClient\Models\WebhookCall */
    public $webhookCall;

    public function __construct(WebhookCall $webhookCall)
    {
        $this->webhookCall = $webhookCall;
    }

    public function handle()
    {
        $price = $this->webhookCall->payload['data']['object'];
        $priceToUpdate = Price::where('stripe_price_id', $price['id'])->first();

        if ($priceToUpdate) {
            $priceToUpdate->price = $price['unit_amount'];
            $priceToUpdate->billing_period = $price['recurring']['interval'];
            $priceToUpdate->active = $price['active'];
            $priceToUpdate->currency = $price['currency'];
            $priceToUpdate->nickname = $price['nickname'];
            $priceToUpdate->save();
        }
    }
}
