<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Product;

class Price extends Model
{
    use HasFactory;

    protected $fillable = [
        'stripe_price_id', 'product_id', 'price',
        'currency', 'nickname', 'active', 'billing_period'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
