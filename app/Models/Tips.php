<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tips extends Model
{
    use HasFactory;

    protected $fillable = [
        'ticker', 'type', 'expiration_date', 'strike', 'last_option_price',
        'underlying_price', 'volume', 'open_interest', 'volume_open_interest',
        'volume_usd', 'volume_on_ask'
    ];
}
