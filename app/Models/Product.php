<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    
    protected $fillable = ['id_stripe_product', 'name', 'active', 'include', 'not_include'];

    protected $casts = [
        'include' => 'array',
        'not_include' => 'array',
    ];
}
