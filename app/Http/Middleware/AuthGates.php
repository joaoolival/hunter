<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Models\Feature;
use Illuminate\Support\Facades\Log;


class AuthGates
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {



        if (Auth::check()) {

            $userFeatures = Feature::select('features.name')
                ->join('feature_price', 'feature_price.feature_id', '=', 'features.id')
                ->join('prices', 'feature_price.price_id', '=', 'prices.id')
                ->join('subscriptions', 'prices.stripe_price_id', '=', 'subscriptions.stripe_plan')
                ->where('subscriptions.user_id', Auth::id())
                ->where('subscriptions.stripe_status', 'active')
                ->where(function ($query) {
                    return $query->whereNull('subscriptions.ends_at')
                        ->orWhere('subscriptions.ends_at', '>', now()->toDateTimeString());
                })
                ->pluck('features.name');

            foreach ($userFeatures as $feature) {
                Gate::define($feature, function () {
                    return true;
                });
            }
        }
        return $next($request);
    }
}
