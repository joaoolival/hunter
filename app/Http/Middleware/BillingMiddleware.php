<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class BillingMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        dd("billing middleware not used");
        $user = $request->user();

        if ($user && !$user->subscribred('default')) {
            return redirect('subscription');
        }

        return $next($request);
    }
}
