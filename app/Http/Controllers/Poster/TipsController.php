<?php

namespace App\Http\Controllers\Poster;

use Illuminate\Http\Request;
use App\Models\Tips;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class TipsController extends Controller
{

    public function store(Request $request)
    {
        if ($request->is('api/*')) {
            $validator = Validator::make($request->all(), [
                'ticker' => 'required',
                'type' => 'required',
                'expiration_date' => 'required',
                'last_option_price' => 'required',
                'underlying_price' => 'required',
                'open_interest' => 'required',
                'volume_open_interest' => 'required',
                'volume_usd' => 'required',
                'volume_on_ask' => 'required',
            ]);




            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()]);
            } else {
                $tip = Tips::create($request->all());
                if ($tip) {
                    return response()->json(['status' => 'success'], 200);
                }
            }
        }
        abort(401, 'Unautenthicated');
    }
}
