<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Tips;
use Illuminate\Http\Request;

class TipsController extends Controller
{
    public function index(Request $request)
    {

        $this->authorize('tips');
        $tips = Tips::all();
        if ($request->is('api/*')) {
            return $tips;
        } else {
            return view('user.tips.index', compact('tips'));
        }
    }
}
