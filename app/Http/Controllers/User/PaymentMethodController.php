<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentMethodController extends Controller
{


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $intent = Auth::user()->createSetupIntent();
        return view('user.payment-methods.create', compact('intent'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Auth::user()->addPaymentMethod($request->input('payment-method'));
            if ($request->input('default') == 1) {
                Auth::user()->updateDefaultPaymentMethod($request->input('payment-method'));
            }
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage());
        }

        return redirect()->route('billing')->withMessage('Payment method added successfuly');
    }


    public function markDefault(Request $request, $paymentMethod)
    {
        try {
            Auth::user()->updateDefaultPaymentMethod($paymentMethod);
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage());
        }

        return redirect()->route('billing')->withMessage('Payment method updated successfuly');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
