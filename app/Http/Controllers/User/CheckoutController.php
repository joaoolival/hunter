<?php

namespace App\Http\Controllers\User;

use App\Models\Country;
use Illuminate\Support\Facades\Auth;
use App\Models\Price;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CheckoutController extends Controller
{
    public function checkout($price_id)
    {
        $price = Price::findOrFail($price_id);
        $countries = Country::all();
        $currentPlan = Auth::user()->subscription('default');
        $currentPrice = $currentPlan->stripe_plan ?? NULL;

        if (!is_null($currentPrice) && ($currentPrice != $price->stripe_price_id || $currentPlan->stripe_status != 'active')) {
            try {
                Auth::user()->subscription('default')->swapAndInvoice($price->stripe_price_id);
                return redirect()->route('billing');
            } catch (\Exception $e) {
                return redirect()->back()->withError($e->getMessage());
            }
        }

        $intent = Auth::user()->createSetupIntent();
        return view('user.billing.checkout', compact('price', 'intent', 'countries'));
    }

    public function processCheckout(Request $request)
    {
        $price = Price::findOrFail($request->input('billing_plan_id'));

        try {
            Auth::user()->newSubscription('default', $price->stripe_price_id)
                ->create($request->input('payment-method'));

            Auth::user()->update([
                //'trial_ends_at' => NULL,
                'company_name' => $request->input('company_name'),
                'address_line_1' => $request->input('address_line_1'),
                'address_line_2' => $request->input('address_line_2'),
                'country_id' => $request->input('country_id'),
                'city' => $request->input('city'),
                'zipcode' => $request->input('zipcode'),

            ]);

            return redirect()->route('billing')->withMessage('Subscribed successfully');
        } catch (\Exception $e) {
            return redirect()->back()->withError($e->getMessage());
        }
    }
}
