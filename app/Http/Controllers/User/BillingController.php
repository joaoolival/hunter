<?php

namespace App\Http\Controllers\User;

use App\Models\Payment;
use Illuminate\Support\Facades\Auth;
use App\Models\Price;
use App\Http\Controllers\Controller;


class BillingController extends Controller
{
    public function index()
    {
        $prices = Price::all();

        $currentPrice = Auth::user()->subscription('default') ?? NULL;


        $paymentMethods = NULL;
        $defaultPaymentMethod = NULL;
        if (!is_null($currentPrice)) {
            $paymentMethods = Auth::user()->paymentMethods();
            $defaultPaymentMethod = Auth::user()->defaultPaymentMethod();
        }


        $payments = Payment::where('user_id', Auth::id())->latest()->get();
        return view('user.billing.index', compact(
            'prices',
            'currentPrice',
            'paymentMethods',
            'defaultPaymentMethod',
            'payments'
        ));
    }

    public function cancel()
    {
        Auth::user()->subscription('default')->cancel();
        return redirect()->route('billing');
    }

    public function resume()
    {
        Auth::user()->subscription('default')->resume();
        return redirect()->route('billing');
    }

    public function downloadInvoice($paymentId)
    {
        $payment = Payment::where('user_id', Auth::id())->where('id', $paymentId)->firstOrFail();
        $filename = storage_path('app/invoices/' . $payment->id . '.pdf');
        if (!file_exists($filename)) {
            abort(404);
        }
        return response()->download($filename);
    }
}
