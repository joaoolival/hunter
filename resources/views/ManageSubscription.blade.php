@extends('layouts.app')

@section('content')

    <span>Manage Subscription</span><br>
    <h2>Hang on! You need an active Sub to proceed</h2>
    <a href="">Head to Checkout</a>
    <span>{{ $stripeKey }}</span><br>
    <span>{{ $checkoutSessionId }}</span><br>
@endsection
