@extends('layouts.app')



@section('content')
@guest
Welcome - No Account Signed in
@endguest
@auth
Welcome - {{ Auth::user()->name }}
@endauth

@endsection