@extends('layouts.app')

@section('content')
<div class="container-xl mt-3">
    <div class="row justify-content-center">

        <div class="card">
            <div class="card-header">All Tips</div>
            <div class="card-body">
                <table class="table">
                    <thead>
                        <tr>
                            <th>Ticker</th>
                            <th>Type</th>
                            <th>Expiration Date</th>
                            <th>Strike</th>
                            <th>Last OP</th>
                            <th>UPrice</th>
                            <th>Volume</th>
                            <th>OI</th>
                            <th>Volume OI</th>
                            <th>Volume USD</th>
                            <th>Volume On Ask</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($tips as $tip)
                        <tr>
                            <td>{{ $tip->ticker }} </td>
                            <td>{{ $tip->type }} </td>
                            <td>{{ $tip->expiration_date }} </td>
                            <td>{{ $tip->strike }} </td>
                            <td>{{ $tip->last_option_price }} </td>
                            <td>{{ $tip->underlying_price }} </td>
                            <td>{{ $tip->volume }} </td>
                            <td>{{ $tip->open_interest }} </td>
                            <td>{{ $tip->volume_open_interest }} </td>
                            <td>{{ $tip->volume_usd }} </td>
                            <td>{{ $tip->volume_on_ask }} </td>

                        </tr>

                        @empty
                        <tr>
                            <td colspan="2">No tips found.</td>
                        </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</div>
@endsection