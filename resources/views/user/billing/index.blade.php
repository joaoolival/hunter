@extends('layouts.app')

@section('content')
<div class="container my-4">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <!-- MY PLAN ACTIVE SUBSCRIPTIONS-->
            <div class="card">
                <div class="card-header">
                    My Plan
                </div>
                <div class="card-body">
                    @if (session('message'))
                    <div class="alert alert-info"> {{ session('message') }}</div>
                    @endif


                    @if (is_null($currentPrice))
                    You are now in a free plan. Please Subscribe to enjoy our services: <br /><br />
                    @elseif($currentPrice->trial_ends_at)
                    <div class=" alert alert-info">
                        Your trial ends on {{ $currentPrice->trial_ends_at->toDateString() }}.
                    </div>
                    @elseif($currentPrice->stripe_status != 'active')
                    <div class=" alert alert-info">
                        Your subscription payment failed. Please update your payment details.
                        <a href="{{ route('cashier.payment', $currentPrice->latestPayment()->id) }}">
                            Please confirm your payment.
                        </a>
                    </div>
                    @endif
                    <hr />
                    <br>
                    <div class="row justify-content-center">
                        @foreach ($prices as $price)
                        <div class="col-md-4 text-center">
                            <h3>{{ $price->product->name }}</h3>
                            <b>{{ number_format($price->price / 100, 2) }}$ per {{$price->billing_period}}</b>
                            <br>
                            @foreach($price->product->include as $include)
                            <p>{{ $include }} <i class="fa fa-check text-success" aria-hidden="true"></i></p>
                            @endforeach
                            @foreach($price->product->not_include as $not_include)
                            <p>{{ $not_include }} <i class="fas fa-times text-danger" aria-hidden="true"></i></p>
                            @endforeach
                            <hr>
                            <br>
                            @if (!is_null($currentPrice) && $price->stripe_price_id == $currentPrice->stripe_plan && $currentPrice->stripe_status == 'active')
                            Your current plan.
                            <br>
                            @if (!$currentPrice->onGracePeriod())
                            <a href="{{ route('cancel') }}" class="btn btn-danger" onclick="return confirm('Are you sure?')"> Cancel Plan</a>
                            @else
                            Your subscrition ends on {{ $currentPrice->ends_at->toDateString() }}
                            <br>
                            <a href="{{ route('resume') }}" class="btn btn-primary">Resume
                                Subscription</a>
                            @endif

                            @else
                            <a href="{{ route('checkout', $price->id) }}" class="btn btn-primary">
                                Subscribe to a {{ $price->product->name }}
                            </a>
                            @endif
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
            @if (!is_null($currentPrice))
            <br>
            <!-- PAYMENT METHODS-->
            <div class="card">
                <div class="card-header">
                    Payment methods
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Card Brand</th>
                                <th>Card Number</th>
                                <th>Expires at</th>
                                <th>Default Card</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($paymentMethods as $paymentMethod)
                            <tr>
                                <td>{{ $paymentMethod->card->brand }}</td>
                                <td>**** **** **** {{ $paymentMethod->card->last4 }}</td>
                                <td>{{ $paymentMethod->card->exp_month }} /
                                    {{ $paymentMethod->card->exp_year }}
                                </td>
                                <td>
                                    @if ($defaultPaymentMethod->id == $paymentMethod->id)
                                    default
                                    @else
                                    <a href="{{ route('payment-methods.markDefault', $paymentMethod->id) }}">Mark
                                        as default</a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <br>
                    <a href="{{ route('payment-methods.create') }}" class="btn btn-primary">Add Payment
                        Method</a>
                </div>
            </div>
            @endif
            <br>
            <!-- PAYMENT METHODS-->
            <div class="card">
                <div class="card-header">
                    Payment History
                </div>
                <div class="card-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Payment Date</th>
                                <th>Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($payments as $payment)
                            <tr>
                                <td>{{ $payment->created_at }}</td>
                                <td>${{ number_format($payment->total /  100 , 2) }}</td>
                                <td>
                                    <a class="btn btn-sm btn-primary" href="{{ route('invoices.download', $payment->id)}}">Download Invoice</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection