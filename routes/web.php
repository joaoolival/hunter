<?php

use App\Http\Controllers\User\BillingController;
use App\Http\Controllers\User\CheckoutController;
use App\Http\Controllers\User\PaymentMethodController;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use App\Http\Controllers\User\TipsController as TipsControllerUser;
use App\Http\Controllers\Poster\TipsController as TipsControllerPoster;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;

require __DIR__ . '/auth.php';

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::middleware(['auth', 'verified'])->group(function () {

    Route::get('billing', [BillingController::class, 'index'])->name('billing');
    Route::get('checkout/{price_id}', [CheckoutController::class, 'checkout'])->name('checkout');
    Route::post('checkout', [CheckoutController::class, 'processCheckout'])->name('checkout.process');
    Route::get('cancel', [BillingController::class, 'cancel'])->name('cancel');
    Route::get('resume', [BillingController::class, 'resume'])->name('resume');
    Route::get('payment-methods/default/{methodId}', [PaymentMethodController::class, 'markDefault'])->name('payment-methods.markDefault');
    Route::resource('payment-methods', PaymentMethodController::class);
    Route::get('invoices/download{invoiceId}', [BillingController::class, 'downloadInvoice'])->name('invoices.download');
    Route::get('tips', [TipsControllerUser::class, 'index'])->name('tips.index');


    Route::group(
        [
            'prefix' => 'poster',
            'middleware' => 'is_poster',
            'as' => 'poster.',
        ],
        function () {
            Route::post('tips', [TipsControllerPoster::class, 'store'])->name('tips.store');
        }
    );
});


/* STRIPE WEBHOOKS*/
Route::stripeWebhooks('stripe-webhook');
