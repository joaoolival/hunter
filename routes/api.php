<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\TipsController as TipsControllerUser;
use App\Http\Controllers\Poster\TipsController as TipsControllerPoster;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\Log;
use App\Models\Role;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/




Route::middleware(['auth:sanctum', 'verified'])->group(function () {


    Route::apiResource('tips', TipsControllerUser::class)->only(['index'])->names([
        'index' => 'api.tips'
    ]);

    Route::group(
        [
            'prefix' => 'poster',
            'middleware' => 'is_poster',
            'as' => 'poster.',
        ],
        function () {
            Route::post('tipsfromr', [TipsControllerPoster::class, 'store'])->name('tips.storefromr');
        }
    );
});


/*

TO GET TOKEN WITH EMAIL AND PASS


Route::post('token', function (Request $request) {

    $request->validate([
        'email' => 'required|email',
        'password' => 'required',
        'device_name' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    if (!$user || !Hash::check($request->password, $user->password) || !($user->role_id == Role::IS_POSTER)) {
        throw ValidationException::withMessages([
            'email' => ['The provided credentials are incorrect.'],
        ]);
    }

    return $user->createToken($request->device_name)->plainTextToken;
});
*/